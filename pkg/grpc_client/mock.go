package grpc_client

import pbu "gitlab.com/medium_clone/medium_api_gateway/genproto/user_service"

func (g *GrpcClient) SetUserService(u pbu.UserServiceClient) {
	g.connections["user_service"] = u
}

func (g *GrpcClient) SetAuthService(u pbu.AuthServiceClient) {
	g.connections["auth_service"] = u
}
