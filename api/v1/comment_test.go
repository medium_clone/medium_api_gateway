package v1_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_clone/medium_api_gateway/api/models"
)

func createComments(t *testing.T) *models.Comment {
	AuthUser := Userlogin(t)
	post := createPosts(t)
	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.CreateComment{
		PostId:      int(post.ID),
		Description: faker.Sentence(),
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/comments", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", AuthUser.AccessToken)

	router.ServeHTTP(resp, req)
	assert.Equal(t, 201, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Comment
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return &response
}

func TestCreateComments(t *testing.T) {
	createComments(t)
}
