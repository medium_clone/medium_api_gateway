package v1_test

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_clone/medium_api_gateway/api/models"
)

func createCategories(t *testing.T) *models.Category {
	AuthUser := Adminlogin(t)
	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.Category{
		Title: faker.Sentence(),
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/categories", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", AuthUser.AccessToken)

	router.ServeHTTP(resp, req)
	assert.Equal(t, 201, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.Category
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return &response
}

func TestCreateCategories(t *testing.T) {
	createCategories(t)
}
