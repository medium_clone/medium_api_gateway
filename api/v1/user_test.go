package v1_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/bxcodec/faker/v4"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	pbu "gitlab.com/medium_clone/medium_api_gateway/genproto/user_service"

	"gitlab.com/medium_clone/medium_api_gateway/api/models"
	"gitlab.com/medium_clone/medium_api_gateway/pkg/grpc_client/mock_grpc"
)

func TestGetAllUsers(t *testing.T) {
	resp := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/users", nil)
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	fmt.Println(resp.Body.String())
}

func TestCreateUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	reqBody := models.CreateUserRequest{
		FirstName: faker.FirstName(),
		LastName:  faker.LastName(),
		Email:     faker.Email(),
		Password:  "@Qwerty123",
		Type:      "superadmin",
		Gender:    "male",
	}

	userService := mock_grpc.NewMockUserServiceClient(ctrl)
	userService.EXPECT().Create(context.Background(), &pbu.User{
		FirstName: reqBody.FirstName,
		LastName:  reqBody.LastName,
		Email:     reqBody.Email,
		Password:  reqBody.Password,
		Type:      reqBody.Type,
		Gender:    reqBody.Gender,
	}).Times(1).Return(&pbu.User{
		Id:        1,
		FirstName: reqBody.FirstName,
		LastName:  reqBody.FirstName,
		Email:     reqBody.Email,
		Type:      reqBody.Type,
		Gender:    reqBody.Gender,
		CreatedAt: time.Now().Format(time.RFC3339),
	}, nil)

	payload, err := json.Marshal(reqBody)
	assert.NoError(t, err)

	grpcConn.SetUserService(userService)
	accessToken := mockAuthMiddleware(t, ctrl)

	req, _ := http.NewRequest("POST", "/v1/users", bytes.NewBuffer(payload))
	req.Header.Add("Authorization", accessToken)

	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	assert.Equal(t, http.StatusCreated, rec.Code)

	body, _ := io.ReadAll(rec.Body)

	var response models.User
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	assert.Equal(t, reqBody.FirstName, response.FirstName)
	assert.Equal(t, reqBody.Email, response.Email)
}
