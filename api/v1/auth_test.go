package v1_test

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/bxcodec/faker/v4"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/medium_clone/medium_api_gateway/api/models"
	pbu "gitlab.com/medium_clone/medium_api_gateway/genproto/user_service"
	"gitlab.com/medium_clone/medium_api_gateway/pkg/grpc_client/mock_grpc"
)

func Userlogin(t *testing.T) *models.AuthResponse {
	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.LoginRequest{
		Email:    "useremail@gmail.com",
		Password: "AdminUkan",
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/auth/login", bytes.NewBuffer(payload))
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return &response
}

func Adminlogin(t *testing.T) *models.AuthResponse {
	resp := httptest.NewRecorder()

	payload, err := json.Marshal(models.LoginRequest{
		Email:    "ukan265@gmail.com",
		Password: "AdminUkan",
	})
	assert.NoError(t, err)
	req, _ := http.NewRequest("POST", "/v1/auth/login", bytes.NewBuffer(payload))
	router.ServeHTTP(resp, req)

	assert.Equal(t, 200, resp.Code)

	body, _ := io.ReadAll(resp.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	return &response
}

func TestLogin(t *testing.T) {
	Userlogin(t)
	Adminlogin(t)
}

func TestLoginMock(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	reqBody := models.LoginRequest{
		Email:    "ukan265@gmail.com",
		Password: "Samandar",
	}

	authService := mock_grpc.NewMockAuthServiceClient(ctrl)
	authService.EXPECT().Login(context.Background(), &pbu.VerifyRequest{
		Email: reqBody.Email,
		Code:  reqBody.Password,
	}).Times(1).Return(&pbu.AuthResponse{
		Id:          1,
		FirstName:   "Samandar",
		LastName:    "To'xtayev",
		Email:       "ukan265@gmail.com",
		Username:    "ukan",
		Type:        "superadmin",
		Password:    "Samandar",
		CreatedAt:   time.Now().Format(time.RFC3339),
		AccessToken: faker.Sentence(),
	}, nil)

	payload, err := json.Marshal(reqBody)
	assert.NoError(t, err)

	grpcConn.SetAuthService(authService)

	req, _ := http.NewRequest("POST", "/v1/auth/login", bytes.NewBuffer(payload))
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	assert.Equal(t, 200, rec.Code)

	body, _ := io.ReadAll(rec.Body)

	var response models.AuthResponse
	err = json.Unmarshal(body, &response)
	assert.NoError(t, err)
	assert.NotEmpty(t, response.AccessToken)
}

func mockAuthMiddleware(t *testing.T, ctrl *gomock.Controller) string {
	accessToken := faker.UUIDHyphenated()

	authService := mock_grpc.NewMockAuthServiceClient(ctrl)

	authService.EXPECT().VerifyToken(context.Background(), &pbu.VerifyTokenRequest{
		AccessToken: accessToken,
		Resource:    "users",
		Action:      "create",
	}).Times(1).Return(&pbu.AuthPayload{
		Id:            faker.UUIDHyphenated(),
		UserId:        1,
		Email:         faker.Email(),
		UserType:      "superadmin",
		HasPermission: true,
		IssuedAt:      time.Now().Format(time.RFC3339),
		ExpiredAt:     time.Now().Add(time.Hour).Format(time.RFC3339),
	}, nil)

	grpcConn.SetAuthService(authService)
	return accessToken
}
